import {LitElement, html} from 'lit-element';
import 'lit-element-bootstrap/components/alert/bs-alert';
import getdomain from 'gly-domain/gly-domain.js';

class ArchiveMessage extends LitElement {
  static get properties() {
    return {
      sampleids: Array,
      accession: String
    };
  }

  render() {
    return html `
  <style>
    padding: 10px; margin-bottom: 10px; border: 1px solid #333333;
  </style>
  <div id="summary">${this._processHtml()}<div>
   `;
  }

  constructor() {
    super();
    console.log("constructor");
    this.accession="";
    this.image="";
    this.sampleids=[];
  }

  connectedCallback() {
    super.connectedCallback();
    console.log("cc");
    const host =  getdomain(location.href);
    const url1 = 'https://'+ host +'/sparqlist/api/gtc-archived-messeage?accession=' + this.accession;
    /* const url1 = 'http://127.0.0.1:8088/sparqlist/api/gtc-archived-messeage?accession=' + this.accession; */
    this.getSummary(url1);
  }

  getSummary(url1) {
    console.log(url1);
    var urls = [];

    urls.push(url1);
    var promises = urls.map(url => fetch(url, {
      mode: 'cors'
    }).then(function (response) {
      return response.json();
    }).then(function (myJson) {
      console.log("summary");
      console.log(JSON.stringify(myJson));

      return myJson;
    }));
    Promise.all(promises).then(results => {
      console.log("values");
      console.log(results);

      this.sampleids = results.pop();
    });
  }

  _processHtml() {
    var popped = this.sampleids.pop();
    if (popped.isArchive) {
      if (popped.PublicLinks.length) {
        /* if (popped.comment.length) { */
      if (popped.comments.length || popped.PublicLinks.length > 1) {
          /* has public link, has reason */
          return html`
          <bs-alert danger>
          <div slot="message">
            This entry has been archived.  The updated version of this entry is at  
            ${popped.PublicNums.map(num =>
              html` <a href="https://${location.host}/Structures/Glycans/${num}" target="_blank">${num}</a>`
            )}
            <p>Reason:</p>
              <ul>
                <!-- Eliminate duplicate comments -->
                ${Array.from(new Set(popped.comments)).map(item => 
                  html`<li>${item}</li>`
                )}
              </ul>
          </div>
          </bs-alert>`
        } else {
          /* has public link, no reason */
          return html`
          <bs-alert danger>
          <div slot="message">
            This entry has been archived.  The updated version of this entry is at 
            <a href="${popped.PublicLinks}">${popped.PublicNums}</a>
          </div>
          </bs-alert>`
        }
      } else {
        if (popped.comments.length) {
          /* no public link, has reason */
          return html`
          <bs-alert danger>
          <div slot="message">
            This entry has been archived.  
            <p>Reason:</p>
              <ul>
                <!-- Eliminate duplicate comments -->
                ${Array.from(new Set(popped.comments)).map(item => 
                  html`<li>${item}</li>`
                )}
              </ul>
          </div>
          </bs-alert>`
        } else {
          /* no public link, no reason */
          return html`
          <bs-alert danger>
          <div slot="message">
            This entry has been archived.
          </div>
          </bs-alert>`
        }
      }
    } else {
      return html``;
    }
  }
}

customElements.define('gtc-archive-message', ArchiveMessage);
