import {LitElement, html} from 'lit-element';
import 'lit-element-bootstrap';

class PartnerLink extends LitElement {

  static get properties() {
    return {
      user: {
        type: String
      },
      key: {
        type: String
      },
      id: {
        type: String
      },
      partner: {
        type: Object
      },
      isPartner: {
        type: Boolean
      }
    };
  }

  render() {
    console.log(this.user + " " + this.key + " " + this.isPartner)
    if (this.user === "" || this.key === "" || !this.isPartner) {
      return html`<!-- ${this.isPartner} -->`;
    } else {
      return html`<a href="/Partner/Glycans/${this.id}">Hello Partner! Manage data for this entry</a>`;
    } 
  }

  constructor() {
    super();
    this.user = "";
    this.key = "";
    this.partner = {};
    this.isPartner = false;
  }

  async connectedCallback() {
    super.connectedCallback();
    if ("" !== this.user && "" !== this.key) {
      this.partner = await this.checkPartner(this.user, this.key);
      console.log("partner:");
      console.log(this.partner);
      console.log(this.partner.message);
      let partnerJson = JSON.parse(this.partner.message);
      console.log(partnerJson.GtcPartner)
      if (null !== partnerJson.GtcPartner)
        this.isPartner = true;
    }
    console.log("this.isPartner");
    console.log(this.isPartner);
  }

  async checkPartner(userid, key) {
    var url = "/glycan/partner/check";
    var auth = window.btoa(userid + ':' + key);
    const results = await fetch(url, {
      method: 'GET',
      mode: 'cors',
      cache: 'default',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': "Basic " + auth
      }
    }).then(function (response) {
        return response.json();
    }).catch(function(err) {
      return JSON.parse('{ "error":"system error >' + err + '<" }')
    });

    return results;
  }

}

customElements.define('partner-link', PartnerLink);

