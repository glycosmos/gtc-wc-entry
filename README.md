# GlyTouCan Entry page Web Components  

The main component used in GTC's new Glycan Entry page.

Current entry page [Example](https://test.gtc.glycosmos.org/Structures/Glycans/G00838JF)


# How to start?

Install node packages.
```
$ npm i
```

A main webcomponent file is in public/ directory. To update public, enter webpack command.
```
$ webpack
```

Open the index on a browser
```
$ live-server
or
$ live-server &
$ jobs
$ kill %1
```

brows url

http://127.0.0.1:8080/public/?id=G02640VC#test

## Update each webcomponents
* if each webcomponents version updated, needs run `npm update`
* needs to update this webcomponent version in `package.json`.

# Add to the test env  
* if you update view on a entry page, you MUST run the dev-gly-gtc-component pipline.
* https://gitlab.com/glycosmos/cdn/dev-gly-gtc-component/pipelines

## Updates

### 2020/11/19

Modify entry component to have user and key attributes.
Add partner-link to link to partner based on login credentials.
