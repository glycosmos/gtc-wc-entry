import {LitElement, html} from 'lit-element';
import 'gly-image/gly-image.js';
import 'gtc-wc-summary/gtc-wc-summary.js';
import 'gtc-wc-wurcs/gtc-wc-wurcs.js';
import 'gtc-wc-glycoct/gtc-wc-glycoct.js';
import 'gly-validated-message/gly-gtc-vm-accession.js';
import './gtc-wc-citation.js';
import 'gtc-wc-external/gtc-wc-external.js';
import 'gtc-wc-literature/gtc-wc-literature.js';
import 'lit-element-bootstrap';
import './gtc-archive-message.js';
import './partner-link.js';
import getdomain from 'gly-domain/gly-domain.js';

class GtcWcEntry extends LitElement {
  static get properties() {
    return {
      sampleids: Array,
      accession: String,
      style: {
        type: String,
        notify: true
      },
      format: {
        type: String,
        notify: true
      },
      notation: {
        type: String,
        notify: true
      },
      user: String,
      key: String,
    };
  }

  render() {
    return html `
    <style>
    .entryNew {
      position: relative;
      width: 1000px;
      margin: 0 auto;
    }

    .entryNew_summary {
      margin: 50px 0;
      padding: 20px;
      border-radius: 10px;
      background: #EEE;
    }

    .entrySummary {
      margin: 10px 0 0;
      padding: 10px;
      background: #FFF;
    }

    .entrySummary_item {
      margin: 10px 0 0;
    }

    .entryNew_heading {
      margin: 0 0 20px;
      padding: 0 0 5px;
      border-bottom: solid 1px #999;
      font-size: 18px;
      font-weight: bold;
    }

    .entryNew_heading-2nd {
      margin: 20px 0 0 30px;
      padding: 10px 0 0;
      font-size: 16px;
      font-weight: bold;
    }

    .entryNew_section {
      margin: 0 0 20px;
      padding: 0 0 30px;
    }

    pre {
    	display: block;
    	margin: 0 0 0 20px;
    	padding: 20px 0 10px;
    	/* border: solid 1px #CCC; */
    	background: #FFF;
    	word-break: break-all;
    	white-space: normal;
    }
    </style>
    ${this._processHtml()}
   `;
  }

  constructor() {
    super();
    console.log("constructor");
    this.accession="G54245YQ";
    this.image="";
    this.sampleids=[];
    this.imagestyle="extended";
    this.format="png";
    this.notation="snfg";
    this.user="";
    this.key="";
  }

  connectedCallback() {
    super.connectedCallback();

    // const url1 = 'https://test.sparqlist.glycosmos.org/sparqlist/api/gtc-archived-messeage?accession=' + this.accession;
    // this.getSummary(url1);
    this.accession = this._getParam('id');
      console.log('this.accession:  ' + this.accession);
  }

  _getParam(name) {
      let url = location.href;
      // console.log('url:  ' + url);
      name = name.replace(/[\[\]]/g, "\\$&");
      let regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)");
      let results = regex.exec(url);
      // if (!results) return null;
      // if (!results[2]) return '';
      console.log('results:  ' + results);
      if(results === null){
        let acc =  url.split('/').slice(-1)[0];
            console.log('acc:  ' + acc);
        if(acc.startsWith('G')){
            return acc;
        }
      }
      return results === null ? '' : decodeURIComponent(results[2].replace(/\+/g, ""));
  }

  // getSummary(url1) {
  //   console.log(url1);
  //   var urls = [];
  //
  //   urls.push(url1);
  //   var promises = urls.map(url => fetch(url, {
  //     mode: 'cors'
  //   }).then(function (response) {
  //     return response.json();
  //   }).then(function (myJson) {
  //     console.log("summary");
  //     console.log(JSON.stringify(myJson));
  //
  //     return myJson;
  //   }));
  //   Promise.all(promises).then(results => {
  //     console.log("values");
  //     console.log(results);
  //
  //     this.sampleids = results.pop();
  //   });
  // }

  _processHtml() {

    return html`
<gtc-archive-message accession="${this.accession}"></gtc-archive-message>
<bs-container>
  <div class="entryNew">
    <section class="entryNew_summary">
      <div class="entrySummary">
        <h1 class="entryNew_heading">${this.accession}</h1>
        <!-- Summary -->
        <gly-image accession="${this.accession}" style="${this.imagestyle}" notation="${this.notation}" format="${this.format}"></gly-image>
        <!-- The Menu can be linked with properties in gtc-wc-image:
        <gtc-wc-menu-image style="{{style}}" notation="{{notation}}" format="{{format}}"></gtc-wc-menu-image> -->
        <div class="entrySummary_item">
          <gtc-wc-summary accession="${this.accession}"></gtc-wc-summary>
        </div>
        <div class="entrySummary_item">
          <gtc-wc-citation class="entrySummary_item" accession="${this.accession}"></gtc-wc-citation>
        </div>
      </div>
    </section>
    <div class="entryContents">
      <!-- Publication -->
      <section id="literature" class="entryNew_section">
        <h1 class="entryNew_heading">Literature</h1>
        <gtc-wc-literature accession="${this.accession}"></gtc-wc-literature>
        <h2 class="entryNew_heading entryNew_heading-2nd">
        <a href="/Registries/supplement/${this.accession}">Register your Publication!</a>
        </h2>
      </section>
      <!-- External ID -->
      <section class="entryNew_section">
        <h1 class="entryNew_heading">External ID</h1><partner-link user="${this.user}" key="${this.key}" id="${this.accession}"></partner-link>
        <gtc-wc-external accession="${this.accession}"></gtc-wc-external>
      </section>
      <section class="entryNew_section">
      </section>
      <!-- Computed Descriptors -->
      <section class="entryNew_section">
        <h1 class="entryNew_heading">Computed Descriptors</h1>
        <h2 class="entryNew_heading entryNew_heading-2nd">WURCS</h2>
        <pre><gtc-wc-wurcs accession="${this.accession}"></gtc-wc-wurcs></pre>
        <h2 class="entryNew_heading entryNew_heading-2nd">GlycoCT</h2>
        <pre><gtc-wc-glycoct accession="${this.accession}"></gtc-wc-glycoct></pre>
      </section>
    </div>
  </div> <!-- entryNew -->
</bs-container>
    `;
  }

}

customElements.define('gtc-wc-entry', GtcWcEntry);
