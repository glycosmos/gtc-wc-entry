import {LitElement, html} from 'lit-element';

class GtcWcCitation extends LitElement {
  static get properties() {
    return {
      accession: String
    };
  }
  constructor() {
    super();
    console.log("constructor");
    this.accession="";
    this.image="";
    this.sampleids=[];
    this.contributionTime=null;
  }

  render() {
    return html `
<style>
.citation {
	margin: 5px 0;
	padding: 0;
}

.citation_content {
	display: none;
	margin: 5px 0 10px;
	padding: 20px;
	background: #FFC;
}

.citation_text {
	margin: 0;
	padding: 0;
	color: #787878;
	font-size: 14px;
}

.citation_copy {
	display: block;
	width: 100px;
	margin: 10px 0 0 auto;
	padding: 0;
	text-align: right;
	color: #999;
	font-size: 14px;
	text-decoration: underline;
}

.citation_btn {
	display: block;
	width: 150px;
	margin: 0 0 0 auto;
	padding: 5px 0;
	border: solid 2px #CCC;
	background: #FFF;
	color: #999;
	font-size: 14px;
	font-weight: bold;
	text-align: center;
	text-decoration: none;
	cursor: pointer;
}

.citation_btn:hover {
	background: #EEE;
}

.toggleBox_label {
  cursor: pointer;
}

.toggleBox_checkbox {
  display: none;
}

.toggleBox_checkbox + .toggleBox_content {
  display: none;
}

.toggleBox_checkbox:checked + .toggleBox_content {
  display: block;
}
</style>
<div class="citation">
  <label for="ch1" class="citation_btn toggleBox_label">Cite this record</label>
  <input id="ch1" class="toggleBox_checkbox" type="checkbox" />
	<div class="citation_content toggleBox_content">
		<p class="citation_text">International Glycan Structure Repository. GlyTouCan; Accession Number=${this.accession}, https://glytoucan.org/Structures/Glycans/${this.accession} (accessed ${this.accessedDate()}).</p>
	</div>
</div>
   `;
  }

  accessedDate() {
    let now = new Date();
    let date = now.toString().slice(4, 7) + '. ' + now.getDate() + ', ' + now.getFullYear();
    return date;
  }


  connectedCallback() {
    super.connectedCallback();
    console.log("cc");
    // const url1 = 'https://test.sparqlist.glycosmos.org/sparqlist/api/gtc_summary?accNum=' + this.accession;
    // const url2 = 'https://test.sparqlist.glycosmos.org/sparqlist/api/gtc_image?accession=' + this.accession + '&style=normalinfo&notation=snfg&format=svg';

    // const url1 = 'https://test.sparqlist.glycosmos.org/sparqlist/api/gtc_summary?accNum=' + this.accession;
    // const url2 = 'https://test.sparqlist.glycosmos.org/sparqlist/api/gtc_image?accession=' + this.accession + '&style=normalinfo&notation=snfg&format=svg';
    // this.getSummary(url1);
  }

//   getSummary(url1) {
//     console.log(url1);
//     var urls = [];

//     urls.push(url1);
//     var promises = urls.map(url => fetch(url, {
//       mode: 'cors'
//     }).then(function (response) {
//       return response.json();
//     }).then(function (myJson) {
//       console.log("summary");
//       console.log(JSON.stringify(myJson));
//       return myJson;
//     }));
//     Promise.all(promises).then(results => {
//       console.log("values");
//       console.log(results);

//       this.sampleids = results.pop();
//     });
//   }

//   _processHtml() {
//     if (this.sampleids.length > 0) {
//       var popped = this.sampleids.pop();
//       // <p>Accession number:  ${item.AccessionNumber}</p>
//       // <p>Calculated Monoisotopic Mass: ${item.Mass}</p>
//       // <p>Contribution time: ${item.ContributionTime}</p>
//       return html`<p>Accession number: ${popped.AccessionNumber}</p><p>Calculated Monoisotopic Mass: ${popped.Mass}</p><p>Contribution time: ${popped.ContributionTime}</p>`;

//     } else {
//       return html`Could not retrieve`;
//     }
//   }
}

customElements.define('gtc-wc-citation', GtcWcCitation);
